import 'dart:math';
import 'package:face_change_app/pages/page_index.dart';
import 'package:flutter/material.dart';

void main() => runApp(
  MaterialApp(
    home: PageIndex(),
  )
);

class PageChangeFace extends StatefulWidget {
  const PageChangeFace({
    super.key,
    required this.friendName
  });

  final String friendName;

  @override
  State<PageChangeFace> createState() => _CharacterState();

}

class _CharacterState extends State<PageChangeFace> {
  int characterNumber1 = Random().nextInt(2) + 1;
  int characterNumber2 = Random().nextInt(1) + 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _build(context),
    );
  }

  Widget _build(BuildContext context) {
    return Center(
        child: TextButton(
            onPressed: () {
              setState(() {
                characterNumber1 = Random().nextInt(2) + 1;
                characterNumber2 = Random().nextInt(1) + 1;
              });
            },
            child: Stack(
              children: <Widget>[
                Image.asset('assets/eyebrow$characterNumber1.png'),
                Image.asset('assets/eyes$characterNumber1.png'),
                Image.asset('assets/face$characterNumber2.png'),
                Image.asset('assets/mouse$characterNumber2.png'),
                Image.asset('assets/nose$characterNumber1.png'),
              ],
            )
        )
    );
  }
}