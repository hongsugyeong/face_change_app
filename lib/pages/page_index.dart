import 'package:face_change_app/pages/page_make_face.dart';
import 'package:face_change_app/pages/page_change_face.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('친구 얼굴 만들기'),
        ),
        body: FormBuilder(
          key: _formKey,
          child: Column(
            children: [
              FormBuilderTextField(
                name: 'friendName',
                decoration: const InputDecoration(labelText: '친구이름'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              MaterialButton(
                color: Theme.of(context).colorScheme.secondary,
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    String friendName = _formKey.currentState!.fields['friendName']!.value;
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageChangeFace(friendName: 'friendNdma')));
                  }
                },
                child: const Text('Login'),
              )
            ],
          ),
        ),
    );
  }
}
